/** Header */

function showModalMenu(){
    var modal = document.getElementById("modal-menu");
    modal.style.display = "block";
}

function closeModalMenu(){
    var modal = document.getElementById("modal-menu");
    modal.setAttribute('closing',true);
    modal.addEventListener('animationend',() =>{
        modal.removeAttribute('closing');
        modal.style.display = "none";
    },{once:true})
}

function darkmodeOn(){
    console.log('ok');
    var isDarkMode = false;
    var elements = document.querySelectorAll('.add-darkmode-here');
    var darkmodeIcon = document.getElementById('darkmodeIcon');
    for(var element of elements){
        if(!element.classList.contains('dark-mode')){
            element.classList.add('dark-mode');
            isDarkMode = true;
        } else {
            element.classList.remove('dark-mode');
        }
    }
    if(isDarkMode){
        darkmodeIcon.innerHTML = 'light_mode';
    } else {
        darkmodeIcon.innerHTML = 'dark_mode';
    }
}


/**Middle */

$(".counter").countUp({
    time: 2000,
    delay: 10,
  });
  
  $(document).ready(function () {
    $(".popup-with-zoom-anim").magnificPopup({
      type: "inline",
      fixedContentPos: false,
      fixedBgPos: true,
      overflowY: "auto",
      closeBtnInside: true,
      preloader: false,
      midClick: true,
      removalDelay: 300,
      mainClass: "my-mfp-zoom-in"
    });
  });
  